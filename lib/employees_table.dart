import 'package:flutter/material.dart';

import 'employee_cell.dart';
import 'employee.dart';

class EmployeesTable extends StatelessWidget {
  const EmployeesTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(constraints: const BoxConstraints(maxWidth: 500.0),
    child: Table(
      columnWidths: const <int, TableColumnWidth> {
        0:FixedColumnWidth(100) // 2 = index of column (count from 0)
      },
      defaultColumnWidth: const FlexColumnWidth(), // flex column อื่นที่ไม่ได้ fix ตามพื้นที่ที่เหลือ
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: <TableRow>[
        TableRow(
          decoration: BoxDecoration(
            color: Colors.green[200],
          ),
          children: const <Widget>[
            EmployeeCell(title: 'Name', isHeader: true),
            EmployeeCell(title: 'Role', isHeader: true),
            EmployeeCell(title: 'Hourly rate', isHeader: true),
          ],
        ),
        ...Employee.getEmployees().map((employee) {
          return TableRow(children: [
            EmployeeCell(title: employee.name),
            EmployeeCell(title: employee.role),
            EmployeeCell(title: '\$ ${employee.hourlyRate}'),
          ]);
        }),
      ],
    ),
    );

  }
}